# PromedicusClient

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.

## Installation

Unfortuantly, I don't know a maven like way to install this client, so we've some convoluted hoops to jump through...

Firstly, check out this project into a handy local repo.

Then, in your IDE of choice, create a new angular project.
In the process of creating the new project you will (hopefully automatically) execute the following commands:
npm install @angular/cli@7.3.8
ng new [PROJECT_NAME] --skip-git
npm install --save-dev angular-ide

When prompted "Would you like to add Angular routing? (y/N)" select 'y'
You'll then be prompted to select stylesheet format. Select CSS

when that completes you may need to open a new terminal window. Either way, execute:
npm install bootstrap jquery --save

Once that finishes:
copy angular.json from root of git project into root of generated project, 
replacing existing angular.json

copy package.json from root of git project into root of generated project, 
replacing existing package.json

copy proxy.conf.json from root of git project into root of generated project.

copy contents of ./src/app from git project into ./src/app of generated project.

You should now be ready to run the client, following instructions for starting a local server below.

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`.
This will read proxy settings from proxy.conf.json, allowing us to connect to the expected service endpoint `http://localhost:8080`.
The app will automatically reload if you change any of the source files.
