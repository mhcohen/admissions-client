import { Admission } from '../admission';
import { AdmissionService } from '../admission.service';
import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { IsFuture } from '../_helpers/IsFuture';
import { Sexes } from '../_helpers/Sexes';
import { Categories } from '../_helpers/Categories';

@Component({
  selector: 'app-edit-admission',
  templateUrl: './edit-admission.component.html',
  styleUrls: ['./edit-admission.component.css']
})
export class EditAdmissionComponent implements OnInit {

  editAdmissionForm: FormGroup;

  admission: Admission;
  submitted = false;

  sexes: Array<string> = Sexes();
  categories: Array<string> = Categories();

  constructor(private admissionService: AdmissionService, private route: ActivatedRoute, private formBuilder: FormBuilder) { }

  ngOnInit() {
      this.route.params.subscribe(params => {
          const id = 'id';
          this.admissionService.getAdmission(params[id]).subscribe(res => {
              this.admission = res;
          });
      });
      this.editAdmissionForm = this.formBuilder.group({
          name: ['', [Validators.required, Validators.minLength(3)]],
          birthdate: ['', Validators.required],
          sex: ['', Validators.required],
          category: ['', Validators.required]
      }, {
          validator: IsFuture('birthdate')
      });
  }

  save() {
      this.admissionService.updateAdmission(this.admission.id, this.admission)
        .subscribe(data => console.log(data), error => console.log(error));

      this.admission = new Admission();
  }

  get f() {
    return this.editAdmissionForm.controls;
  }

  onSubmit() {
      this.submitted = true;
      this.admission.source = 'Admissions App';
      this.save();
  }

}
