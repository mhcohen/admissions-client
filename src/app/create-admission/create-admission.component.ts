import { AdmissionService } from '../admission.service';
import { Admission } from '../admission';
import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { IsFuture } from '../_helpers/IsFuture';
import { Sexes } from '../_helpers/Sexes';
import { Categories } from '../_helpers/Categories';

@Component({
  selector: 'app-create-admission',
  templateUrl: './create-admission.component.html',
  styleUrls: ['./create-admission.component.css']
})
export class CreateAdmissionComponent implements OnInit {

  creatAdmissionForm: FormGroup;

  admission: Admission = new Admission();
  submitted = false;

  sexes: Array<string> = Sexes();
  categories: Array<string> = Categories();

  constructor(private admissionService: AdmissionService, private formBuilder: FormBuilder) { }

  ngOnInit() {
      this.creatAdmissionForm = this.formBuilder.group({
        name: ['', [Validators.required, Validators.minLength(3)]],
        birthdate: ['', Validators.required],
        sex: ['', Validators.required],
        category: ['', Validators.required]
      }, {
        validator: IsFuture('birthdate')
      });
  }

  newAdmission(): void {
      this.submitted = false;
      this.admission = new Admission();
  }

  save() {
      this.admissionService.createAdmission(this.admission)
        .subscribe(data => console.log(data), error => console.log(error));

      this.admission = new Admission();
  }

  get f() {
    return this.creatAdmissionForm.controls;
  }

  onSubmit() {
      this.submitted = true;
      this.admission.source = 'Admissions Application';
      this.save();
  }

}
