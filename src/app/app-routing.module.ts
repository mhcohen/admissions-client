import { EditAdmissionComponent } from './edit-admission/edit-admission.component';
import { CreateAdmissionComponent } from './create-admission/create-admission.component';
import { AdmissionListComponent } from './admission-list/admission-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    { path: '', redirectTo: 'admissions', pathMatch: 'full'},
    { path: 'admissions', component: AdmissionListComponent },
    { path: 'admissions/add', component: CreateAdmissionComponent},
    { path: 'admissions/update/:id', component: EditAdmissionComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
