import { Observable } from 'rxjs';
import { AdmissionService } from '../admission.service';
import { Admission } from '../admission';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admission-list',
  templateUrl: './admission-list.component.html',
  styleUrls: ['./admission-list.component.css']
})
export class AdmissionListComponent implements OnInit {

  admissions: Observable<Admission[]>;

  constructor(private admissionService: AdmissionService) { }

  ngOnInit() {
      this.reloadData();
  }

  reloadData() {
      this.admissions = this.admissionService.getAdmissionsList();
  }

  deleteAdmission(id: number, admission: Admission) {
      if (confirm('Are you sure you want to delete the admission of  ' + admission.name)) {
      this.admissionService.deleteAdmission(id)
        .subscribe(
            data => {
                console.log(data);
                this.reloadData();
            },
            error => console.log(error));
        }
  }

}
