import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdmissionService {

  private baseUrl = '/api/v1/admissions';

  constructor(private http: HttpClient) { }

  // tslint:disable-next-line:ban-types
  getAdmission(id: number): Observable<any> {
      return this.http.get(`${this.baseUrl}/${id}`);
  }

  // tslint:disable-next-line:ban-types
  createAdmission(admission: Object): Observable<Object> {
      return this.http.post(`${this.baseUrl}`, admission);
  }

  // tslint:disable-next-line:ban-types
  updateAdmission(id: number, value: any): Observable<Object> {
      return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deleteAdmission(id: number): Observable<any> {
      return this.http.delete(`${this.baseUrl}/${id}`, {responseType: 'text'});
  }

  getAdmissionsList(): Observable<any> {
      return this.http.get(`${this.baseUrl}`);
  }

}
