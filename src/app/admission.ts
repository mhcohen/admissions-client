export class Admission {
    id: number;
    dateOfAdmission: string;
    name: string;
    birthDate: string;
    sex: string;
    category: string;
    source: string;
}
