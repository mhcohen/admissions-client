export function Sexes() {
    const sexes: Array<string> = [
        'FEMALE',
        'MALE',
        'INTERSEX',
        'UNKNOWN'
    ];

    return sexes;
}
