export function Categories() {
    const categories: Array<string> = [
        'NORMAL',
        'INPATIENT',
        'EMERGENCY',
        'OUTPATIENT'
    ];

    return categories;
}
