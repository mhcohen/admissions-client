import { FormGroup } from '@angular/forms';

export function IsFuture(controlName: string) {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];

        if (control.errors && !control.errors.notFuture) {
            // Other validations for inDate have already failed
            return;
        }

        const then: Date = new Date(control.value);
        const now: Date = new Date();

        if (then > now) {
            control.setErrors({ isFuture: true });
        } else {
            control.setErrors(null);
        }
    };
}
